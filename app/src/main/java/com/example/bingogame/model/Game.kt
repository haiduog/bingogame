package com.example.bingogame.model

import java.io.Serializable

class Game(
    var winner: String = "",
    var size: String = "",
    var data: ArrayList<Int>? = null,
    var player: ArrayList<Player>? = ArrayList()
) : Serializable