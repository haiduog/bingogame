package com.example.bingogame.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bingogame.R
import java.util.ArrayList

class ResultAdapter(var listResult: ArrayList<String>) :
    RecyclerView.Adapter<ResultAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_result, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvResult.setText(listResult[position])
    }

    override fun getItemCount(): Int = listResult.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val tvResult: TextView = itemView.findViewById(R.id.tv_result)
    }
}