package com.example.bingogame.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bingogame.R
import com.example.bingogame.model.NumberData

class GameAdapter(var listNumber: ArrayList<NumberData>, var iOnClickNumber: IOnClickNumber) :
    RecyclerView.Adapter<GameAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_number, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (listNumber[position].status) {
            holder.btnNumber.setBackgroundResource(R.drawable.bg_number_selected)
        } else {
            holder.btnNumber.setBackgroundResource(R.drawable.bg_number)
        }
        holder.btnNumber.text = listNumber[position].values.toString()
        holder.btnNumber.setOnClickListener {
            holder.btnNumber.setBackgroundResource(R.drawable.bg_number_selected)
            iOnClickNumber.onClick(listNumber[position].values, position)
        }
    }

    override fun getItemCount(): Int = listNumber.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val btnNumber: TextView = itemView.findViewById(R.id.btn_number)
    }

    public interface IOnClickNumber {
        fun onClick(value: Int, position: Int)
    }
}