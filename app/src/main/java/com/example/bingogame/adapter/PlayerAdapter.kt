package com.example.bingogame.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bingogame.R
import com.example.bingogame.model.Player
import java.util.*

class PlayerAdapter(var listPlayer: ArrayList<Player>, var nameUser: String) :
    RecyclerView.Adapter<PlayerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_player, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvStt.text = "$position."
        if (listPlayer[position].status) {
            holder.tvName.setTextColor(Color.RED)
            holder.tvStt.setTextColor(Color.RED)
        } else {
            holder.tvName.setTextColor(Color.BLACK)
            holder.tvStt.setTextColor(Color.BLACK)
        }
        if (nameUser == listPlayer[position].name) {
            holder.tvName.text = listPlayer[position].name + " - You"
        } else {
            holder.tvName.text = listPlayer[position].name
        }
    }

    override fun getItemCount(): Int = listPlayer.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public var tvStt: TextView = itemView.findViewById(R.id.tv_stt)
        public var tvName: TextView = itemView.findViewById(R.id.tv_name)
    }
}
