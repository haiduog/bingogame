package com.example.bingogame.view.game

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bingogame.Constant.URL_DB
import com.example.bingogame.R
import com.example.bingogame.adapter.GameAdapter
import com.example.bingogame.adapter.GameAdapter.IOnClickNumber
import com.example.bingogame.adapter.PlayerAdapter
import com.example.bingogame.adapter.ResultAdapter
import com.example.bingogame.model.Game
import com.example.bingogame.model.NumberData
import com.example.bingogame.model.Player
import com.google.firebase.database.*
import java.util.*
import kotlin.collections.ArrayList


class GameActivity : AppCompatActivity() {
    private lateinit var mArrayNumber: Array<Array<NumberData?>>
    private val RESULT = "MBCCSPRO"
    private lateinit var mListNumber: ArrayList<NumberData>
    private lateinit var mListPlayer: ArrayList<Player>
    private var mListPlayerIntent = ArrayList<Player>()
    private lateinit var mListResult: ArrayList<String>
    private lateinit var mGameAdapter: GameAdapter
    private lateinit var mPlayerAdapter: PlayerAdapter
    private lateinit var mResultAdapter: ResultAdapter
    private lateinit var rcvGame: RecyclerView
    private lateinit var rcvPlayer: RecyclerView
    private lateinit var rcvResult: RecyclerView
    private var mNumberCell = 0
    private var mCheck = 0
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mGame: Game
    private var mIdRoom = ""
    private var mName = ""
    private lateinit var mListNumberValue: ArrayList<Boolean>
    private lateinit var tvRoom: TextView
    private var mIsCreateGame = false
    private lateinit var viewSub: View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        bindView()
        getDataIntent()
        initData()
        initRecyclePlayer()
        initRecycleNumber()
        initRecycleResult()
        initListener()
        initArray()

    }

    private fun initListener() {
        findViewById<Button>(R.id.btn_random_number).setOnClickListener {
            getListNumber()
        }
    }

    private fun initRecycleNumber() {
        mGameAdapter = GameAdapter(mListNumber, object : IOnClickNumber {
            override fun onClick(value: Int, position: Int) {
                nextPlayerStep()
                mListNumber[position].status = true
                mListNumber[position] = mListNumber[position]
                initArray()
                checkArray()
                mDatabase.child(mIdRoom).child("data").child(value.toString()).setValue(0)
            }
        })
        rcvGame.layoutManager =
            GridLayoutManager(this, mNumberCell, GridLayoutManager.VERTICAL, false)
        rcvGame.adapter = mGameAdapter
    }

    private fun initRecyclePlayer() {
        mPlayerAdapter = PlayerAdapter(mListPlayer, mName)
        rcvGame.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcvPlayer.adapter = mPlayerAdapter
    }

    private fun initRecycleResult() {
        mResultAdapter = ResultAdapter(mListResult)
        rcvResult.layoutManager =
            GridLayoutManager(this, mNumberCell, GridLayoutManager.VERTICAL, false)
        rcvResult.adapter = mResultAdapter
    }

    private fun getDataIntent() {
        mName = intent.getStringExtra("name").toString();
        mIdRoom = intent.getStringExtra("id_room").toString()
        mNumberCell = intent.getStringExtra("among")!!.toInt()
        mIsCreateGame = intent.getBooleanExtra("create_game", false)
        try {
            var listPlayer: ArrayList<Player>? =
                intent.getSerializableExtra("list_player") as ArrayList<Player>
            listPlayer?.let { mListPlayerIntent.addAll(it) }
        } catch (e: Exception) {

        }
    }

    private fun initData() {
        mDatabase = FirebaseDatabase.getInstance(URL_DB).getReference("ex");
        mListPlayer = ArrayList()
        mListNumber = ArrayList()
        mListResult = ArrayList()
        mListNumberValue = ArrayList()
        mArrayNumber = Array(mNumberCell) {
            arrayOfNulls<NumberData>(
                mNumberCell
            )
        }
        getRandomNonRepeatingIntegers(
            mNumberCell * mNumberCell,
            mNumberCell * mNumberCell
        ).let { mListNumber.addAll(it) }
        if (mListPlayerIntent.size != 0) {
            mListPlayer.addAll(mListPlayerIntent)
        }
        mListPlayer.add(Player(mName, mIsCreateGame))
        mDatabase.child(mIdRoom).child("player").setValue(mListPlayer)
        mDatabase.child(mIdRoom).child("size").setValue(mNumberCell)


        mDatabase.child(mIdRoom).child("data").addValueEventListener(
            object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (data in snapshot.children) {
                        for (item in mListNumber) {
                            if (data.key.toString() == item.values.toString())
                                item.status = true;
                        }
                    }
                    initArray()
                    mGameAdapter.notifyDataSetChanged()
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })

        mDatabase.child(mIdRoom).child("player").addValueEventListener(
            object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    mListPlayer.clear()
                    val listPlayerRepo: List<Player> = snapshot.children.map { dataSnapShot ->
                        dataSnapShot.getValue(Player::class.java)!!
                    }
                    listPlayerRepo.let { mListPlayer.addAll(it) }
                    mPlayerAdapter.notifyDataSetChanged()

                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }
        )
        tvRoom.text = "Room: $mIdRoom"
    }

    private fun initArray() {
        mArrayNumber = Array(mNumberCell) {
            arrayOfNulls<NumberData>(
                mNumberCell
            )
        }
        var j: Int = 0
        var i: Int = 0
        for (k in 0 until mListNumber.size) {
            if (i == mNumberCell) {
                return
            }
            mArrayNumber[i][j] = mListNumber[k]
            if (j == mNumberCell - 1) {
                j = 0
                i++
            } else {
                j++
            }
        }

    }

    private fun checkArray() {
        mCheck = 0
        for (i in 0 until mNumberCell) {
            var checkRow: Int = 0
            for (j in 0 until mNumberCell) {
                if (mArrayNumber[i][j]?.status == true) {
                    checkRow++
                }
            }
            if (checkRow == mNumberCell) {
                mCheck++
            }
        }

        for (j in 0 until mNumberCell) {
            var checkCell: Int = 0
            for (i in 0 until mNumberCell) {
                if (mArrayNumber[i][j]?.status == true) {
                    checkCell++
                }
            }
            if (checkCell == mNumberCell) {
                mCheck++
            }
        }
        var checkMainDiagonal: Int = 0
        for (i in 0 until mNumberCell) {
            if (mArrayNumber[i][i]?.status == true) {
                checkMainDiagonal++
            }
        }
        if (checkMainDiagonal == mNumberCell) {
            mCheck++
        }
        var checkSupDiagonal: Int = 0
        for (i in 0 until mNumberCell) {
            var checkSuppDiagonal: Int = 0
            for (j in 0 until mNumberCell) {
                if ((i + j == mNumberCell - 1) && mArrayNumber[i][j]?.status == true) {
                    checkSuppDiagonal++
                }
            }
            if (checkSuppDiagonal == 1) {
                checkSupDiagonal++
            }
        }
        if (checkSupDiagonal == mNumberCell) {
            mCheck++
        }

        if (mCheck >= mNumberCell) {
            showDialog()
        }
        addDataResult()

    }

    private fun addDataResult() {
        mListResult.clear()
        for (i in 0 until mCheck) {
            if (mListResult.size < mCheck) {
                mListResult.add(RESULT[i].toString())
            }
        }
        mResultAdapter.notifyDataSetChanged()
    }

    private fun bindView() {
        rcvGame = findViewById(R.id.rcv_number)
        rcvPlayer = findViewById(R.id.rcv_player)
        rcvResult = findViewById(R.id.rcv_result)
        tvRoom = findViewById(R.id.tv_room)
    }

    private fun getRandomInt(max: Int): Int {
        val random = Random()
        return random.nextInt(max) + 1
    }

    private fun getRandomNonRepeatingIntegers(
        size: Int, max: Int
    ): ArrayList<NumberData> {
        val numbers = ArrayList<NumberData>()
        val numbersTemp = ArrayList<Int>()
        while (numbers.size < size) {
            val random = getRandomInt(max)
            if (!numbersTemp.contains(random)) {
                numbers.add(NumberData(random, false))
                numbersTemp.add(random)
            }
        }
        return numbers
    }

    private fun getListNumber() {
        if (mListNumber.size > 0) {
            mListNumber.clear()
            mGame.data?.clear()
        }
        getRandomNonRepeatingIntegers(
            mNumberCell * mNumberCell,
            mNumberCell * mNumberCell
        ).let { mListNumber.addAll(it) }

        mGameAdapter.notifyDataSetChanged()
    }

    private fun showDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Winner")
            .setCancelable(false)
            .setMessage("Chúc mừng bạn")
            .setNegativeButton(
                "Ok"
            ) { p0, _ -> p0?.dismiss() }
        var alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    private fun nextPlayerStep() {
        var step = 0
        for (i in 0 until mListPlayer.size) {
            if (mListPlayer[i].status) {
                step = if (i == mListPlayer.size - 1) {
                    0
                } else {
                    i + 1
                }
                mListPlayer[i].status = false
                break
            }
        }
        mListPlayer[step].status = true
        mDatabase.child(mIdRoom).child("player").setValue(mListPlayer)

    }
}