package com.example.bingogame.view.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bingogame.model.Game

class MainViewModel : ViewModel() {
    private val mRepo = MainRepo()
    private val _mGameInfoLiveData = MutableLiveData<Game>()
    val mGameLiveData: LiveData<Game> = _mGameInfoLiveData

    fun getGameInfo(idRoom: String) {
        mRepo.getInfoGame(idRoom, _mGameInfoLiveData)
    }
}