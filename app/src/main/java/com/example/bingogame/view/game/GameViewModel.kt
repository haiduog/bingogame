package com.example.bingogame.view.game

import androidx.lifecycle.ViewModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class GameViewModel : ViewModel() {
    private val dataRef: DatabaseReference =
        FirebaseDatabase.getInstance().reference.child("bingo-game-28907-default-rtdb")
}