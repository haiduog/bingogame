package com.example.bingogame.view.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.bingogame.Constant.URL_DB
import com.example.bingogame.model.Game
import com.example.bingogame.model.Player
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MainRepo {
    private val mDataBase = FirebaseDatabase.getInstance(URL_DB)
    private val mRef = mDataBase.getReference("ex")


    fun getInfoGame(idRoom: String, liveData: MutableLiveData<Game>) {
        mRef.child(idRoom).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var game = Game()
                game.size = snapshot.child("size").value.toString()
                var listPlayer = snapshot.child("player").children.map {
                    it.getValue(Player::class.java)!!
                }
                game.player?.addAll(listPlayer)
                liveData.postValue(game)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("Firebase", "Failed to read value.", error.toException());
            }

        })
    }
}