package com.example.bingogame.view.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.bingogame.R
import com.example.bingogame.view.game.GameActivity

class MainActivity : AppCompatActivity() {
    private lateinit var edtName: EditText
    private lateinit var edtRoom: EditText
    private lateinit var edtAmong: EditText
    private lateinit var mViewModel: MainViewModel
    private var mOpenGame: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindView()
        implementListener()
    }

    private fun implementListener() {
        mViewModel.mGameLiveData.observe(this) { game ->
            if (game.size == "null" || game.size == "") {
                Toast.makeText(this@MainActivity, "Room empty", Toast.LENGTH_SHORT).show()
            } else if (!mOpenGame) {
                mOpenGame = true;
                Intent(this, GameActivity::class.java).apply {
                    putExtra("name", edtName.text.toString())
                    putExtra("id_room", edtRoom.text.toString())
                    putExtra("among", game.size)
                    putExtra("list_player", game.player)
                    putExtra("create_game", false)
                    startActivity(this)
                }
            }
        }
    }

    private fun bindView() {
        edtName = findViewById(R.id.edt_your_name)
        edtRoom = findViewById(R.id.edt_number_room)
        edtAmong = findViewById(R.id.edt_cell)
        mViewModel = ViewModelProvider(this)[MainViewModel::class.java]

    }

    fun createRoom(v: View) {
        if (edtName.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter yourname", Toast.LENGTH_SHORT).show()
            return
        }
        if (edtRoom.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter room", Toast.LENGTH_SHORT).show()
            return
        }
        Intent(this, GameActivity::class.java).apply {
            putExtra("name", edtName.text.toString())
            putExtra("id_room", edtRoom.text.toString())
            putExtra("among", edtAmong.text.toString())
            putExtra("create_game", true)
            startActivity(this)
        }
    }

    fun joinRoom(v: View) {
        if (edtName.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter yourname", Toast.LENGTH_SHORT).show()
            return
        }
        if (edtRoom.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter room", Toast.LENGTH_SHORT).show()
            return
        }

        mViewModel.getGameInfo(edtRoom.text.toString())

    }
}